'use strict';

// NOTE: This file is used in a MediaWiki context as well, and so MUST parse as a
// stand-alone JS file without use of require()

function checkTypeShallowly( ZObject, keys ) {
	if ( ZObject === null || ZObject === undefined ) {
		return false;
	}
	for ( const key of keys ) {
		if ( ZObject[ key ] === undefined ) {
			return false;
		}
	}
	return true;
}

function isZArgumentReference( ZObject ) {
	return checkTypeShallowly( ZObject, [ 'Z1K1', 'Z18K1' ] );
}

function isZFunctionCall( ZObject ) {
	return checkTypeShallowly( ZObject, [ 'Z1K1', 'Z7K1' ] );
}

function isZReference( ZObject ) {
	if ( !checkTypeShallowly( ZObject, [ 'Z1K1', 'Z9K1' ] ) ) {
		return false;
	}
	return ZObject.Z9K1.match( /^Z[1-9]\d*$/ ) !== null;
}

function isZType( ZObject ) {
	if ( !checkTypeShallowly( ZObject, [ 'Z1K1', 'Z4K1', 'Z4K2', 'Z4K3' ] ) ) {
		return false;
	}
	const identity = ZObject.Z4K1;
	if ( isMemberOfDangerTrio( identity ) || isZType( identity ) ) {
		return true;
	}
	return false;
}

/**
 * Finds the Z9/Reference identity of a Z46/Deserializer. If the identity is
 * not eventually given as a Z9/Reference, returns null.
 *
 * @param {Object} Z46 a Deserializer
 * @return {Object|null} the Z46's Z9/Reference identity
 */
function findDeserializerIdentity( Z46 ) {
	if ( Z46 === undefined ) {
		return null;
	}
	if ( isZReference( Z46 ) ) {
		return Z46;
	}
	return findDeserializerIdentity( Z46.Z46K1 );
}

/**
 * Finds the Z9/Reference identity of a Z8/Function. If the identity is
 * not eventually given as a Z9/Reference, returns null.
 *
 * @param {Object} Z8 a Function
 * @return {Object|null} the Z8's Z9/Reference identity
 */
function findFunctionIdentity( Z8 ) {
	if ( Z8 === undefined ) {
		return null;
	}
	if ( isZReference( Z8 ) ) {
		return Z8;
	}
	return findDeserializerIdentity( Z8.Z8K5 );
}

/**
 * Finds the identity of a type. This might be a Function Call (in the case of
 * a generic type), a Reference (in the case of a builtin), or the Z4 itself
 * (in the case of a user-defined type).
 *
 * @param {Object} Z4 a Type
 * @return {Object|null} the Z4's identity
 */
function findIdentity( Z4 ) {
	if ( isZFunctionCall( Z4 ) || isZReference( Z4 ) ) {
		return Z4;
	}
	if ( isZType( Z4 ) ) {
		const identity = findIdentity( Z4.Z4K1 );
		if ( isZReference( identity ) && !isBuiltInType( identity.Z9K1 ) ) {
			return Z4;
		}
		return identity;
	}
	// I guess this wasn't a type.
	return null;
}

/**
 * Finds the Z9/Reference identity of a Z64/Serializer. If the identity is
 * not eventually given as a Z9/Reference, returns null.
 *
 * @param {Object} Z64 a Serializer
 * @return {Object|null} the Z46's Z9/Reference identity
 */
function findSerializerIdentity( Z64 ) {
	if ( Z64 === undefined ) {
		return null;
	}
	if ( isZReference( Z64 ) ) {
		return Z64;
	}
	return findDeserializerIdentity( Z64.Z64K1 );
}

/**
 * Type-checks Z1 against Function Call, Reference, and Argument Reference.
 * Returns the true if Z1 can plausibly be interpreted as belonging to one of
 * those three types, else false.
 *
 * @param {Object} Z1 object to be validated
 * @return {boolean} whether Z1's type is any of the Danger Trio
 */
function isMemberOfDangerTrio( Z1 ) {
	for ( const typeComparisonFunction of [
		isZArgumentReference,
		isZFunctionCall,
		isZReference
	] ) {
		if ( typeComparisonFunction( Z1 ) ) {
			return true;
		}
	}
	return false;
}

function isString( s ) {
	return typeof s === 'string' || s instanceof String;
}

function isArray( a ) {
	return Array.isArray( a );
}

function isObject( o ) {
	return !isArray( o ) && typeof o === 'object' && o !== null;
}

function isKey( k ) {
	// eslint-disable-next-line security/detect-unsafe-regex
	return k.match( /^(Z[1-9]\d*)?K[1-9]\d*$/ ) !== null;
}

function isZid( k ) {
	return k.match( /^Z[1-9]\d*$/ ) !== null;
}

function isReference( z ) {
	// Note that A1 and Q34 are References but K2 isn't.
	return z.match( /^[A-JL-Z][1-9]\d*$/ ) !== null;
}

function isGlobalKey( k ) {
	return k.match( /^Z[1-9]\d*K[1-9]\d*$/ ) !== null;
}

function kidFromGlobalKey( k ) {
	return k.match( /^Z[1-9]\d*(K[1-9]\d*)$/ )[ 1 ];
}

function deepEqual( o1, o2 ) {
	// TODO (T300650): use something more robust
	return JSON.stringify( o1 ) === JSON.stringify( o2 );
}

function deepCopy( o ) {
	return JSON.parse( JSON.stringify( o ) );
}

/**
 * Recursively freezes an object and its properties so that it cannot be
 * modified or extended.
 *
 * Use this function with caution. In the context of ZObjects, this
 * function should be safe. But for other use cases, make sure there are no
 * cycles in the object or properites that are not supposed to be frozen (window).
 *
 * @param {*} o any object.
 * @return {*} the same object that was the input, but frozen.
 */
function deepFreeze( o ) {
	Object.keys( o ).forEach( ( property ) => {
		const child = o[ property ];
		if ( child && ( typeof child === 'object' ) && !Object.isFrozen( child ) ) {
			deepFreeze( child );
		}
	} );
	return Object.freeze( o );
}

/**
 * Create a Z24 / Void object.  (Z24 is the only possible value of the type
 * Z21 / Unit).
 *
 * @param {boolean} canonical whether output should be in canonical form
 * @return {Object} a reference to Z24
 *
 * TODO (T289301): This should read its outputs from a configuration file.
 */
function makeVoid( canonical = false ) {
	if ( canonical ) {
		return 'Z24';
	}
	return { Z1K1: 'Z9', Z9K1: 'Z24' };
}

/**
 * Checks whether the input is a reference to the given ZID.
 *
 * @param {Object | string} ZObject a ZObject
 * @param {string} ZID the ZID to be compared against
 * @return {boolean} true iff ZObject is a reference with the given ZID
 */
function isThisReference( ZObject, ZID ) {
	// Canonical-form reference.
	if ( isString( ZObject ) ) {
		return ZObject === ZID;
	}
	// Normal-form reference.
	let theReference;
	try {
		theReference = ZObject.Z9K1;
	} catch ( e ) {
		// Z9K1 of ZObject could not be accessed, probably because ZObject was undefined.
		return false;
	}
	return theReference === ZID;
}

/**
 * Checks whether the input is a Z24 / Void.  Allows for either canonical
 * or normalized input (corresponding to what makeVoid produces).
 *
 * @param {Object | string} v item to be checked
 * @return {boolean} whether v is a Z24
 */
function isVoid( v ) {
	// Case 1: v is a reference to Z24.
	if ( isThisReference( v, 'Z24' ) ) {
		return true;
	}

	// Case 2: v is an object whose type is the reference Z21.
	// FIXME: This is still not correct. If v.Z1K1 is a Z4 whose Z4K1/Identity is
	// Z21, this should (but won't) return true. We can't use findIdentity here
	// because findIdentity is defined only for normal-form objects.
	let maybeVoidType;
	try {
		maybeVoidType = v.Z1K1;
	} catch ( e ) {
		// Z1K1 of v could not be accessed, probably because v was undefined.
		return false;
	}
	return isThisReference( maybeVoidType, 'Z21' );
}

/**
 * Z9 Reference to Z41 (true).
 *
 * @return {Object} a reference to Z41 (true)
 */
function makeTrue() {
	return { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' }, Z40K1: { Z1K1: 'Z9', Z9K1: 'Z41' } };
}

/**
 * Z9 Reference to Z42 (false).
 *
 * @return {Object} a reference to Z42 (false)
 */
function makeFalse() {
	return { Z1K1: { Z1K1: 'Z9', Z9K1: 'Z40' }, Z40K1: { Z1K1: 'Z9', Z9K1: 'Z42' } };
}

/**
 * Retrieves the head of a ZList.
 *
 * @param {Object} ZList a generic typed list (Z881)
 * @return {Object} the head list element, K1
 */
function getHead( ZList ) {
	return ZList.K1;
}

/**
 * Retrieves the tail of a ZList.
 *
 * @param {Object} ZList a generic typed list (Z881)
 * @return {Array} the tail list element, K2
 */
function getTail( ZList ) {
	return ZList.K2;
}

/**
 * Sets the tail of a ZList.
 *
 * @param {Object} ZList a generic typed list (Z881)
 * @param {Object} newValue object to be set as new tail
 */
function setTail( ZList, newValue ) {
	ZList.K2 = newValue;
}

/**
 * Determines whether an already-validated ZList is empty. Because the list has
 * already been validated, it is sufficient to check for the presence of K1.
 *
 * @param {Object} ZList a generic typed list (Z881)
 * @return {boolean} whether ZList is empty
 */
function isEmptyZList( ZList ) {
	return getHead( ZList ) === undefined;
}

/**
 * Turns a ZList into a simple JS array for ease of iteration.
 *
 * @param {Object} ZList a generic typed list (Z881)
 * @return {Array} an array consisting of all elements of ZList
 */
function convertZListToItemArray( ZList ) {
	if ( ZList === undefined ) {
		console.error( 'convertZListToItemArray called with undefined; please fix your caller' );
		return [];
	}

	let tail = ZList;
	const result = [];
	while ( true ) {
		// FIXME: This should only be called on "an already-validated ZList", which this isn't?
		if ( isEmptyZList( tail ) ) {
			break;
		}
		result.push( getHead( tail ) );
		tail = getTail( tail );
	}
	return result;
}

/**
 * Turns a JS array into a Typed List.
 *
 * @param {Array} array an array of ZObjects
 * @param {string} headKey key to be used for list head (K1)
 * @param {string} tailKey key to be used for list tail (K2)
 * @param {Object} tailType list type
 * @return {Object} a Typed List corresponding to the input array
 */
function convertArrayToZListInternal( array, headKey, tailKey, tailType ) {
	function createTail() {
		return { Z1K1: tailType };
	}
	const result = createTail();
	let tail = result;
	for ( const element of array ) {
		tail[ headKey ] = element;
		tail[ tailKey ] = createTail();
		tail = tail[ tailKey ];
	}
	return result;
}

/**
 * Infers the shared type of an array of normal ZObjects
 *
 * @param {Array} array an array of ZObjects
 * @param {boolean} canonical whether to output in canonical form
 * @return {Object} type of all the elements of the list or Z1
 */
function inferItemType( array, canonical = false ) {
	const { ZObjectKeyFactory } = require( './schema.js' );
	let headType;
	const Z1K1s = new Set();
	for ( const element of array ) {
		Z1K1s.add( ( ZObjectKeyFactory.create( element.Z1K1 ) ).asString() );
	}

	// If inferred type is a resolver type, return Z1 instead
	const resolverTypes = [ 'Z9', 'Z7', 'Z18' ];
	if ( ( Z1K1s.size === 1 ) && ( !resolverTypes.includes( Z1K1s.values().next().value ) ) ) {
		headType = array[ 0 ].Z1K1;
	} else {
		headType = 'Z1';
	}

	return ( isString( headType ) && !canonical ) ? { Z1K1: 'Z9', Z9K1: headType } : headType;
}

/**
 * Turns a JS array of items into a Typed List after inferring the element type.
 *
 * @param {Array} array an array of ZObjects
 * @param {boolean} canonical whether to output in canonical form
 * @return {Object} a Typed List corresponding to the input array
 */
function convertItemArrayToZList( array, canonical = false ) {
	const headType = inferItemType( array, canonical );
	return convertArrayToKnownTypedList( array, headType, canonical );
}

/**
 * Turns a benjamin array into a Typed List. The benjamin array is an array
 * where the first item describes the types of the following ZObjects.
 *
 * @param {Array} array an array of ZObjects
 * @param {boolean} canonical whether to output in canonical form
 * @return {Object} a Typed List corresponding to the input array
 */
function convertBenjaminArrayToZList( array, canonical = false ) {
	const headType = array.length >= 1 ? array[ 0 ] : ( canonical ? 'Z1' : { Z1K1: 'Z9', Z9K1: 'Z1' } );
	return convertArrayToKnownTypedList( array.slice( 1 ), headType, canonical );
}

/**
 * Turns a JS array into a Typed List of a known type.
 *
 * @param {Array} array an array of ZObjects
 * @param {string} type the known type of the typed list
 * @param {boolean} canonical whether to output in canonical form
 * @return {Object} a Typed List corresponding to the input array
 */
function convertArrayToKnownTypedList( array, type, canonical = false ) {
	const listType = getTypedListType( type, canonical );
	return convertArrayToZListInternal( array, 'K1', 'K2', listType );
}

/**
 * Creates the type value of a Typed List given the expected type of its elements.
 *
 * @param {Object|string} elementType for the list, in normal form
 * @param {boolean} canonical whether to output in canonical form
 * @return {Object} the type of a typed list where the elements are the given type
 */
function getTypedListType( elementType, canonical = false ) {
	const listType = {
		Z1K1: canonical ? 'Z7' : wrapInZ9( 'Z7' ),
		Z7K1: canonical ? 'Z881' : wrapInZ9( 'Z881' )
	};

	// elementType can be a string or an object
	// If it's a string, the type is a canonical reference
	// If it's an object, it may be:
	// 1. a normal reference, e.g. { Z1K1: Z9, Z9K1: Z6 }
	// 2. a canonical function call, e.g. { Z1K1: Z7, Z7K1: Z885, Z885K1: Z500 }
	// 3. a normal function call, e.g. {Z1K1:{ Z1K1: Z9, Z9K1: Z7 }, Z7K1:{ Z1K1: Z9, Z9K1: Z999 }}
	if ( isString( elementType ) ) {
		listType.Z881K1 = canonical ? elementType : wrapInZ9( elementType );
	} else {
		if ( elementType.Z1K1 === 'Z9' ) {
			listType.Z881K1 = canonical ? elementType.Z9K1 : elementType;
		} else {
			// FIXME (T304619): If the type is described by a function call,
			// it could be either normal or canonical
			listType.Z881K1 = elementType;
		}
	}

	return listType;
}

/**
 * Create a new, empty ZMap with the given valueType.
 * At present, the key type of a ZMap can only be Z6 / String or Z39 / Key reference.
 * TODO (T302015) When ZMap keys are extended beyond Z6/Z39, update accordingly
 *
 * @param {Object} keyType A Z9 instance in normal form
 * @param {Object} valueType A ZObject in normal form
 * @return {Object} a Z883 / ZMap with no entries, in normal form
 */
function makeEmptyZMap( keyType, valueType ) {
	const allowedKeyTypes = [ 'Z6', 'Z39' ];
	if ( !allowedKeyTypes.includes( keyType.Z9K1 ) ) {
		console.error( 'makeEmptyZMap called with invalid keyType' );
		return undefined;
	}
	const mapType = {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z883' },
		Z883K1: keyType,
		Z883K2: valueType
	};
	// The map's K1 property is a list of pairs, and it's required to be present
	// even when empty
	const listType = {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z881' },
		Z881K1: {
			Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
			Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' },
			Z882K1: keyType,
			Z882K2: valueType
		}
	};
	return {
		Z1K1: mapType,
		K1: { Z1K1: listType }
	};
}

/**
 * Create a new, empty ZMap for responses in a Z22/ResponseEnvelope.
 *
 * @return {Object} a Z883 / ZMap with no entries, in normal form
 */
function makeEmptyZResponseEnvelopeMap() {
	return makeEmptyZMap(
		{ Z1K1: 'Z9', Z9K1: 'Z6' },
		{ Z1K1: 'Z9', Z9K1: 'Z1' }
	);
}

/**
 * If an object has an .asJSON() method, calls that method and returns the result;
 * otherwise, returns the original object intact.
 *
 * This is a last resort function, to be called sparingly. If we are calling this
 * function, it means we have received a ZWrapper from function-orchestrator
 * code.
 *
 * @param {Object} ZObject a ZObject which may be a ZWrapper
 * @return {Object} the ZObject intact or converted to bare JSON
 */
function maybeAsJSON( ZObject ) {
	try {
		return ZObject.asJSON();
	} catch ( e ) {
		return ZObject;
	}
}

/**
 * Does a quick check to determine if the given ZObject is a Z883 / Map.
 * Does not validate the ZObject (i.e., assumes the ZObject is well-formed).
 *
 * @param {Object} ZObject a Z1/ZObject, in canonical or normal form
 * @return {boolean}
 */
function isZMap( ZObject ) {
	const { ZObjectKeyFactory } = require( './schema.js' );
	return ZObjectKeyFactory.create( maybeAsJSON( ZObject.Z1K1 ) ).ZID_ === 'Z883';
}

/**
 * Ensures there is an entry for the given key / value in the given ZMap.  If there is
 * already an entry for the given key, overwrites the corresponding value.  Otherwise,
 * creates a new entry. N.B.: Modifies the value of the ZMap's K1 in place.
 *
 * TODO (T302015) When ZMap keys are extended beyond Z6/Z39, update accordingly
 *
 * @param {Object} ZMap a Z883/Typed map, in normal form
 * @param {Object} key a Z6 or Z39 instance, in normal form
 * @param {Object} value a Z1/ZObject, in normal form
 * @param {Function} callback a function to call on new entries before returning
 * @return {Object} the updated ZMap, in normal form
 */
function setZMapValue( ZMap, key, value, callback = null ) {
	if ( ZMap === undefined ) {
		console.error( 'setZMapValue called with undefined; please fix your caller' );
		return undefined;
	}

	let penultimateTail = null;
	let tail = ZMap.K1;
	while ( true ) {
		if ( isEmptyZList( tail ) ) {
			break;
		}
		const entry = getHead( tail );
		if ( ( entry.K1.Z1K1 === 'Z6' && key.Z1K1 === 'Z6' && entry.K1.Z6K1 === key.Z6K1 ) ||
			( entry.K1.Z1K1 === 'Z39' && key.Z1K1 === 'Z39' && entry.K1.Z39K1.Z9K1 === key.Z39K1.Z9K1 ) ) {
			entry.K2 = value;
			return ZMap;
		}
		penultimateTail = tail;
		tail = getTail( tail );
	}

	// The key isn't present in the map, so add an entry for it
	const ZMapType = findIdentity( ZMap.Z1K1 );
	const listType = tail.Z1K1;
	const keyType = ZMapType.Z883K1;
	const valueType = ZMapType.Z883K2;
	const pairType = {
		Z1K1: { Z1K1: 'Z9', Z9K1: 'Z7' },
		Z7K1: { Z1K1: 'Z9', Z9K1: 'Z882' },
		Z882K1: keyType,
		Z882K2: valueType
	};
	let newTail = {
		Z1K1: listType,
		K1: {
			Z1K1: pairType,
			K1: key,
			K2: value
		},
		K2: {
			Z1K1: listType
		}
	};
	if ( callback !== null ) {
		newTail = callback( newTail );
	}

	if ( penultimateTail === null ) {
		ZMap.K1 = newTail;
	} else {
		setTail( penultimateTail, newTail );
	}
	return ZMap;
}

/**
 * Return the ZMap value corresponding to the given key, if present.
 * TODO (T302015) When ZMap keys are extended beyond Z6/Z39, update accordingly
 *
 * @param {Object} ZMap a Z883/Typed map, in normal OR canonical form
 * @param {Object} key a Z6 or Z39 instance, in normal OR canonical form (but same form as ZMap)
 * @return {Object} a Z1/Object, the value of the map entry with the given key,
 * or undefined if there is no such entry
 */
function getZMapValue( ZMap, key ) {
	if ( ZMap === undefined ) {
		console.error( 'getZMapValue called with undefined; please fix your caller' );
		return undefined;
	}
	if ( isArray( ZMap.K1 ) ) {
		return getValueFromCanonicalZMap( ZMap, key );
	}

	let tail = ZMap.K1;
	while ( tail !== undefined ) {
		if ( isEmptyZList( tail ) ) {
			break;
		}
		const entry = getHead( tail );

		if ( ( entry.K1.Z1K1 === 'Z6' && key.Z1K1 === 'Z6' && entry.K1.Z6K1 === key.Z6K1 ) ||
			( entry.K1.Z1K1 === 'Z39' && key.Z1K1 === 'Z39' && entry.K1.Z39K1.Z9K1 === key.Z39K1.Z9K1 ) ) {
			return entry.K2;
		}
		tail = getTail( tail );
	}
	return undefined;
}

/**
 * Return the ZMap value corresponding to the given key, if present.
 * INTERNAL to this file; external callers use getZMapValue.
 * TODO (T302015) When ZMap keys are extended beyond Z6/Z39, update accordingly
 *
 * @param {Object} ZMap a Z883/Typed map, in canonical form
 * @param {Object} key a Z6 or Z39 instance, in canonical form
 * @return {Object} a Z1/Object, the value of the map entry with the given key,
 * or undefined if there is no such entry
 */
function getValueFromCanonicalZMap( ZMap, key ) {
	const K1Array = ZMap.K1;
	for ( let i = 1; i < K1Array.length; i++ ) {
		const entry = K1Array[ i ];
		if ( ( entry.K1 === key ) ||
			( entry.K1.Z1K1 === 'Z6' && key.Z1K1 === 'Z6' && entry.K1.Z6K1 === key.Z6K1 ) ||
			( entry.K1.Z1K1 === 'Z39' && key.Z1K1 === 'Z39' && entry.K1.Z39K1 === key.Z39K1 ) ) {
			return entry.K2;
		}
	}
	return undefined;
}

/**
 * Creates a map-based Z22 containing result and metadata.  metadata is normally a Z883 / Map.
 * However, if metadata is a Z5 / Error object, we place it in a new ZMap, as the value of an entry
 * with key "errors", as a programming convenience.
 *
 * @param {Object} result Z22K1 of resulting Z22
 * @param {Object} metadata Z22K2 of resulting Z22 - either a Z883 / Map or a Z5 / Error
 * @param {boolean} canonical whether output should be in canonical form
 * @return {Object} a Z22
 */
function makeMappedResultEnvelope( result = null, metadata = null, canonical = false ) {
	let ZMap;
	if ( metadata && !isZMap( metadata ) && ( metadata.Z1K1 === 'Z5' || metadata.Z1K1.Z9K1 === 'Z5' ) ) {
		const keyType = { Z1K1: 'Z9', Z9K1: 'Z6' };
		const valueType = { Z1K1: 'Z9', Z9K1: 'Z1' };
		ZMap = makeEmptyZMap( keyType, valueType );
		setZMapValue( ZMap, { Z1K1: 'Z6', Z6K1: 'errors' }, metadata );
	} else {
		ZMap = metadata;
	}
	let envelopeType;
	if ( canonical ) {
		envelopeType = 'Z22';
	} else {
		envelopeType = {
			Z1K1: 'Z9',
			Z9K1: 'Z22'
		};
	}
	return {
		Z1K1: envelopeType,
		Z22K1: result === null ? makeVoid( canonical ) : result,
		Z22K2: ZMap === null ? makeVoid( canonical ) : ZMap
	};
}

/**
 * Retrieves the Z5/Error, if present, from the given Z22/Evaluation result (envelope).
 *
 * @param {Object} envelope a Z22/Evaluation result (envelope), in normal OR canonical form
 * @return {Object} a Z5/Error if the envelope contains an error; Z24/void otherwise
 */
function getError( envelope ) {
	const metadata = envelope.Z22K2;
	if ( isZMap( metadata ) ) {
		let canonical, key;
		if ( isArray( metadata.K1 ) ) {
			canonical = true;
			key = 'errors';
		} else {
			canonical = false;
			key = { Z1K1: 'Z6', Z6K1: 'errors' };
		}
		let error = getZMapValue( metadata, key, true );
		if ( error === undefined ) {
			error = makeVoid( canonical );
		}
		return error;
	} else { // metadata is Z24/void
		return metadata;
	}
}

/**
 * Ensures there is an entry for the given key / value in the metadata map
 * of the given Z22 / Evaluation result (envelope).  If the envelope has
 * no metadata map, creates one.  If there is already an entry for the given key,
 * overwrites the corresponding value.  Otherwise, creates a new entry.
 * N.B.: May modify the value of Z22K2 and the ZMap's K1 in place.
 *
 * @param {Object} envelope a Z22/Evaluation result, in normal form
 * @param {Object} key a Z6 or Z39 instance, in normal form
 * @param {Object} value a Z1/ZObject, in normal form
 * @return {Object} the updated envelope, in normal form
 */
function setMetadataValue( envelope, key, value ) {
	let zMap = envelope.Z22K2;
	if ( zMap === undefined || isVoid( zMap ) ) {
		zMap = makeEmptyZResponseEnvelopeMap();
	}
	zMap = setZMapValue( zMap, key, value );
	envelope.Z22K2 = zMap;
	return envelope;
}

const builtInTypesArray_ = Object.freeze( [
	'Z1', 'Z11', 'Z12', 'Z14', 'Z16', 'Z17', 'Z18', 'Z2', 'Z20', 'Z21',
	'Z22', 'Z23', 'Z3', 'Z31', 'Z32', 'Z39', 'Z4', 'Z40', 'Z5', 'Z50', 'Z6',
	'Z60', 'Z61', 'Z7', 'Z8', 'Z80', 'Z86', 'Z9', 'Z99'
] );
const builtInTypes_ = new Set( builtInTypesArray_ );

function builtInTypes() {
	return builtInTypesArray_;
}

function isBuiltInType( ZID ) {
	return builtInTypes_.has( ZID );
}

function isUserDefined( ZID ) {
	return !builtInTypes_.has( ZID );
}

function inferType( object ) {
	if ( isString( object ) ) {
		if ( isReference( object ) ) {
			return 'Z9';
		}
		return 'Z6';
	}
	if ( isArray( object ) ) {
		return 'LIST';
	}
	return object.Z1K1;
}

function wrapInZ6( zid ) {
	return {
		Z1K1: 'Z6',
		Z6K1: zid
	};
}

function wrapInZ9( zid ) {
	return {
		Z1K1: 'Z9',
		Z9K1: zid
	};
}

function wrapInKeyReference( key ) {
	return {
		Z1K1: wrapInZ9( 'Z39' ),
		Z39K1: wrapInZ6( key )
	};
}

function wrapInQuote( data ) {
	return {
		Z1K1: wrapInZ9( 'Z99' ),
		Z99K1: data
	};
}

module.exports = {
	builtInTypes,
	convertItemArrayToZList,
	convertBenjaminArrayToZList,
	convertArrayToKnownTypedList,
	convertZListToItemArray,
	findDeserializerIdentity,
	findFunctionIdentity,
	findIdentity,
	findSerializerIdentity,
	inferItemType,
	isMemberOfDangerTrio,
	isZArgumentReference,
	isZFunctionCall,
	isZReference,
	isZType,
	isString,
	isArray,
	isObject,
	isKey,
	isZid,
	isReference,
	isGlobalKey,
	deepEqual,
	deepCopy,
	deepFreeze,
	getHead,
	getTail,
	getTypedListType,
	inferType,
	isBuiltInType,
	isEmptyZList,
	isUserDefined,
	kidFromGlobalKey,
	makeFalse,
	makeMappedResultEnvelope,
	makeTrue,
	makeVoid,
	isVoid,
	wrapInKeyReference,
	wrapInQuote,
	wrapInZ6,
	wrapInZ9,
	makeEmptyZMap,
	makeEmptyZResponseEnvelopeMap,
	isZMap,
	setZMapValue,
	getZMapValue,
	getError,
	setMetadataValue
};
